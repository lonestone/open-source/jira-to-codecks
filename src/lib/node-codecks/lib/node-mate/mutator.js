"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _slicedToArray2 = require("babel-runtime/helpers/slicedToArray");

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _keys = require("babel-runtime/core-js/object/keys");

var _keys2 = _interopRequireDefault(_keys);

exports.default = createMutator;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createMutator(mutations, dispatcher) {
  var _this = this;

  var instance = {};
  (0, _keys2.default)(mutations).forEach(function (fullActionName) {
    var _fullActionName$split = fullActionName.split("."),
        _fullActionName$split2 = (0, _slicedToArray3.default)(_fullActionName$split, 2),
        actionNamespace = _fullActionName$split2[0],
        actionName = _fullActionName$split2[1];

    instance[actionNamespace] = instance[actionNamespace] || {};
    instance[actionNamespace][actionName] = function () {
      var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(data) {
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", dispatcher(actionNamespace + "/" + actionName, data));

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, _this);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();
  });

  instance.$descriptions = mutations;

  return instance;
}
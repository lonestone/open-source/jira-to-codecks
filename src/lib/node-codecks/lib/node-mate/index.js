"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require("babel-runtime/core-js/object/keys");

var _keys2 = _interopRequireDefault(_keys);

var _toConsumableArray2 = require("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

exports.default = createApi;

var _mutator = require("./mutator");

var _mutator2 = _interopRequireDefault(_mutator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var today = new Date();

var defaults = {
  array: [],
  json: {},
  date: today,
  int: 0,
  day: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() }
};

function fieldDescFromDesc() {
  var desc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  return (0, _extends3.default)({
    type: "default",
    defaultValue: defaults[desc.type] !== undefined ? defaults[desc.type] : "unknown",
    deps: []
  }, desc);
}

function normalizeDescription(desc) {
  if (desc.name === "_root") {
    desc.idPropAsArray = [];
  } else {
    desc.idProp = desc.idProp || "id";
    desc.idPropAsArray = Array.isArray(desc.idProp) ? desc.idProp : [desc.idProp];
  }

  desc.fields = [].concat((0, _toConsumableArray3.default)(desc.fields), (0, _toConsumableArray3.default)(desc.idPropAsArray)).reduce(function (fields, field) {
    if (typeof field === "string") {
      fields[field] = fieldDescFromDesc();
    } else {
      (0, _keys2.default)(field).forEach(function (fieldLabel) {
        var fieldDesc = field[fieldLabel];
        fields[fieldLabel] = fieldDescFromDesc(typeof fieldDesc === "string" ? {} : fieldDesc);
      });
    }
    return fields;
  }, {});

  function hasManyDescFromString(key) {
    return { model: key.replace(/s$/, ""), isSingleton: false, fk: desc.name + "Id", deps: [] };
  }

  function belongsToDescFromString(key) {
    return { model: key, fk: key + "Id", deps: [] };
  }

  desc.hasMany = (desc.hasMany || []).reduce(function (m, entry) {
    if (typeof entry === "string") {
      m[entry] = hasManyDescFromString(entry);
    } else {
      (0, _keys2.default)(entry).forEach(function (relName) {
        m[relName] = (0, _extends3.default)({}, hasManyDescFromString(relName), entry[relName]);
      });
    }
    return m;
  }, {});

  desc.belongsTo = (desc.belongsTo || []).reduce(function (m, entry) {
    if (typeof entry === "string") {
      m[entry] = belongsToDescFromString(entry);
    } else {
      (0, _keys2.default)(entry).forEach(function (relName) {
        m[relName] = (0, _extends3.default)({}, belongsToDescFromString(relName), entry[relName]);
      });
    }
    return m;
  }, {});

  return desc;
}

function createApi(descriptionList, fetcher, mutations, dispatcher) {
  var descriptions = descriptionList.reduce(function (m, d) {
    m[d.name] = normalizeDescription((0, _extends3.default)({}, d));
    return m;
  }, {});

  var API = {};

  var mutate = (0, _mutator2.default)(mutations, dispatcher, descriptions);

  API.descriptions = descriptions;
  API.mutate = mutate;
  return API;
}
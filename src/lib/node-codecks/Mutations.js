"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray2 = require("babel-runtime/helpers/slicedToArray");

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _keys = require("babel-runtime/core-js/object/keys");

var _keys2 = _interopRequireDefault(_keys);

var _set = require("babel-runtime/core-js/set");

var _set2 = _interopRequireDefault(_set);

var _objectDestructuringEmpty2 = require("babel-runtime/helpers/objectDestructuringEmpty");

var _objectDestructuringEmpty3 = _interopRequireDefault(_objectDestructuringEmpty2);

var _toConsumableArray2 = require("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mutations = {
  "cards.create": {
    type: "create",
    model: "card",
    implicit: function implicit(_ref) {
      var attachments = _ref.attachments,
          deckId = _ref.deckId,
          userId = _ref.userId;
      return [{ desc: { type: "create", model: "unseenCardDiff" }, data: {} }].concat((0, _toConsumableArray3.default)(!deckId ? [{ desc: { type: "create", model: "card", via: "hand_cards" }, data: { userId: userId } }] : []), (0, _toConsumableArray3.default)(attachments && attachments.length ? [{
        desc: { type: "update", model: "account" },
        data: { totalMediaByteUsage: null }
      }] : []));
    }
  },
  "cards.update": {
    type: "update",
    model: "card",
    implicit: function implicit(_ref2, _ref3) {
      var id = _ref2.id,
          deckId = _ref2.deckId,
          assigneeId = _ref2.assigneeId;
      var _isOptimistic = _ref3._isOptimistic;
      return [{ desc: { type: "create", model: "cardHistory" }, data: { cardId: id } }, { desc: { type: "create", model: "unseenCardDiff" }, data: { cardId: id } }].concat((0, _toConsumableArray3.default)(deckId && !_isOptimistic ? [{ desc: { type: "custom", fn: invalidateDeckHistory } }] : []), (0, _toConsumableArray3.default)(assigneeId && !_isOptimistic ? [{ desc: { type: "custom", fn: invalidateAssigneeHistory } }] : []));
    }
  },
  "cards.bulkUpdate": {
    type: "update",
    model: "card",
    implicit: function implicit(_ref4, _ref5) {
      var ids = _ref4.ids,
          deckId = _ref4.deckId,
          assigneeId = _ref4.assigneeId,
          addMasterTag = _ref4.addMasterTag,
          removeMasterTag = _ref4.removeMasterTag;
      var _isOptimistic = _ref5._isOptimistic;
      return ids.reduce(function (l, id) {
        l.push({ desc: { type: "create", model: "cardHistory" }, data: { cardId: id } }, { desc: { type: "create", model: "unseenCardDiff" }, data: { cardId: id } });
        if (addMasterTag || removeMasterTag) l.push({
          desc: { type: "update", model: "card" },
          data: { id: id, masterTags: undefined }
        });
        return l;
      }, [].concat((0, _toConsumableArray3.default)(deckId && !_isOptimistic ? [{ desc: { type: "custom", fn: invalidateDeckHistory } }] : []), (0, _toConsumableArray3.default)(assigneeId && !_isOptimistic ? [{ desc: { type: "custom", fn: invalidateAssigneeHistory } }] : [])));
    }
  },
  "cards.checkBox": {
    type: "update",
    model: "card",
    implicit: function implicit(_ref6) {
      var id = _ref6.id;
      return [{ desc: { type: "update", model: "card" }, data: { content: undefined, id: id } }, { desc: { type: "create", model: "cardHistory" }, data: { cardId: id } }, { desc: { type: "create", model: "unseenCardDiff" }, data: { cardId: id } }];
    }
  },

  "cards.addFile": {
    type: "create",
    model: "attachment",
    implicit: function implicit(_ref7) {
      var cardId = _ref7.cardId;
      return [{ desc: { type: "update", model: "card" }, data: { attachments: undefined, cardId: cardId } }, { desc: { type: "create", model: "cardHistory" }, data: { cardId: cardId } }, { desc: { type: "create", model: "unseenCardDiff" }, data: { cardId: cardId } }, { desc: { type: "update", model: "account" }, data: { totalMediaByteUsage: undefined } }];
    }
  },

  "decks.create": {
    type: "create",
    model: "deck",
    implicit: function implicit(_ref8, _ref9) {
      var userId = _ref8.userId;
      var id = _ref9.id;
      return [{
        desc: { type: "create", model: "deck", via: "deck_subscriptions" },
        data: { userId: userId, id: id }
      }];
    }
  },
  "decks.update": {
    type: "update",
    model: "deck",
    implicit: function implicit(_ref10) {
      var id = _ref10.id,
          milestoneId = _ref10.milestoneId;
      return [].concat((0, _toConsumableArray3.default)(milestoneId !== undefined ? [{ desc: { type: "update", model: "card" }, data: { deckId: id, milestoneId: milestoneId } }] : []));
    }
  },
  "decks.updateCover": {
    type: "update",
    model: "deck",
    implicit: function implicit(_ref11) {
      var id = _ref11.id;
      return [{ desc: { type: "update", model: "deck" }, data: { coverFileId: undefined, id: id } }];
    }
  },
  "decks.delete": {
    type: "update",
    model: "deck",
    implicit: function implicit(_ref12) {
      var id = _ref12.id;
      return [{ desc: { type: "update", model: "deck" }, data: { isDeleted: true, id: id } }];
    }
  },
  "decks.updateOrder": {
    type: "create",
    model: "deckOrder",
    implicit: function implicit() {
      return [{ desc: { type: "update", model: "deckOrder" }, data: { sortIndex: undefined } }];
    }
  },

  "accounts.update": {
    type: "update",
    model: "account",
    implicit: function implicit(_ref13) {
      var id = _ref13.id;
      return [{
        desc: { type: "update", model: "balance" },
        data: { accountId: id, gift: undefined, actual: undefined, bonus: undefined, includesTaxPercent: undefined }
      }];
    }
  },
  "accounts.disable": {
    type: "update",
    model: "account",
    implicit: function implicit(_ref14) {
      var id = _ref14.id;
      return [{
        desc: { type: "update", model: "account" },
        data: { id: id, disabledBy: undefined, isDisabled: undefined, disabledAt: undefined }
      }, {
        desc: { type: "update", model: "subscription" },
        data: { accountId: id, canceledBy: undefined, status: undefined, canceledAt: undefined }
      }];
    }
  },
  "accounts.updateSetting": {
    type: "update",
    model: "accountSetting"
  },

  "projects.create": {
    type: "create",
    model: "project"
  },
  "projects.update": {
    type: "update",
    model: "project",
    implicit: function implicit() {
      return [{ desc: { type: "update", model: "setting" }, data: { value: undefined } }];
    }
  },
  "projects.setVisibility": {
    type: "update",
    model: "project"
  },
  "projects.updateCover": {
    type: "update",
    model: "project",
    implicit: function implicit(_ref15) {
      var id = _ref15.id;
      return [{ desc: { type: "update", model: "project" }, data: { coverFileId: undefined, id: id } }];
    }
  },
  "projects.setMasterTags": {
    type: "update",
    model: "setting"
  },
  "projects.changeMasterTag": {
    type: "update",
    model: "setting",
    implicit: function implicit() {
      return [{ desc: { type: "update", model: "setting" }, data: { value: undefined } }, { desc: { type: "update", model: "card" }, data: { masterTags: undefined } }];
    }
  },

  "attachments.deleteFile": {
    type: "update",
    model: "attachment",
    implicit: function implicit(_ref16) {
      var fileId = _ref16.fileId;
      return [{
        desc: { type: "update", model: "file" },
        data: { id: fileId, isDeleted: true, deletedBy: undefined, deletedAt: undefined }
      }, { desc: { type: "update", model: "account" }, data: { totalMediaByteUsage: undefined } }];
    }
  },
  "attachments.batchDeleteFile": {
    type: "update",
    model: "attachment",
    implicit: function implicit(_ref17) {
      var fileIds = _ref17.fileIds;
      return fileIds.map(function (fileId) {
        return [{
          desc: { type: "update", model: "file" },
          data: { id: fileId, isDeleted: true, deletedBy: undefined, deletedAt: undefined }
        }, { desc: { type: "update", model: "account" }, data: { totalMediaByteUsage: undefined } }];
      });
    }
  },
  "attachments.update": {
    type: "update",
    model: "attachment"
  },
  "attachments.delete": {
    type: "delete",
    model: "attachment",
    implicit: function implicit(_ref18) {
      var cardId = _ref18.cardId;
      return [{ desc: { type: "update", model: "card" }, data: { attachments: undefined, cardId: cardId } }, { desc: { type: "create", model: "cardHistory" }, data: { cardId: cardId } }, { desc: { type: "create", model: "unseenCardDiff" }, data: { cardId: cardId } }];
    }
  },

  "resolvables.create": {
    type: "create",
    model: "resolvable",
    implicit: function implicit(_ref19) {
      var cardId = _ref19.cardId;
      return [{ desc: { type: "create", model: "resolvableEntry" }, data: { cardId: cardId } }, { desc: { type: "create", model: "undismissedResolvable" }, data: {} }, { desc: { type: "create", model: "undismissedNonParticipatingResolvable" }, data: {} }];
    }
  },
  "resolvables.close": {
    type: "update",
    model: "resolvable",
    implicit: function implicit(_ref20) {
      var id = _ref20.id;
      return [{ desc: { type: "delete", model: "resolvable", via: "vw_snoozing_resolvables" }, data: { id: id } }];
    }
  },
  "resolvables.comment": {
    type: "create",
    model: "resolvableEntry",
    implicit: function implicit(_ref21) {
      var resolvableId = _ref21.resolvableId;
      return [{ desc: { type: "create", model: "resolvableParticipant" }, data: { resolvableId: resolvableId } }, {
        desc: { type: "update", model: "resolvableParticipant" },
        data: { done: undefined, lastChangedAt: undefined, resolvableId: resolvableId }
      }, { desc: { type: "create", model: "undismissedResolvable" }, data: { resolvableId: resolvableId } }, { desc: { type: "create", model: "resolvableParticipantHistory" }, data: { resolvableId: resolvableId } }, { desc: { type: "delete", model: "resolvable", via: "vw_snoozing_resolvables" }, data: { resolvableId: resolvableId } }];
    }
  },
  "resolvables.updateComment": {
    type: "update",
    model: "resolvableEntry",
    implicit: function implicit(_ref22) {
      var entryId = _ref22.entryId,
          resolvableId = _ref22.resolvableId;
      return [{ desc: { type: "create", model: "resolvableParticipant" }, data: { resolvableId: resolvableId } }, {
        desc: { type: "update", model: "resolvableParticipant" },
        data: { done: undefined, lastChangedAt: undefined, resolvableId: resolvableId }
      }, { desc: { type: "create", model: "resolvableParticipantHistory" }, data: { resolvableId: resolvableId } }, { desc: { type: "create", model: "resolvableEntryHistory" }, data: { entryId: entryId } }];
    }
  },
  "resolvables.updateParticipantDone": {
    type: "update",
    model: "resolvableParticipant",
    implicit: function implicit(_ref23) {
      var id = _ref23.id,
          resolvableId = _ref23.resolvableId;
      return [{ desc: { type: "update", model: "resolvableParticipant" }, data: { lastChangedAt: undefined, id: id } }, { desc: { type: "delete", model: "undismissedResolvable" }, data: { resolvableId: resolvableId } }, { desc: { type: "create", model: "resolvableParticipantHistory" }, data: { resolvableId: resolvableId } }, { desc: { type: "delete", model: "resolvable", via: "vw_snoozing_resolvables" }, data: { resolvableId: resolvableId } }];
    }
  },

  "users.update": {
    type: "update",
    model: "user",
    implicit: function implicit(_ref24) {
      var id = _ref24.id,
          profileImageData = _ref24.profileImageData;
      return profileImageData ? [{ desc: { type: "update", model: "user" }, data: { profileImageId: undefined, id: id } }] : [];
    }
  },
  "users.seenRelease": {
    type: "update",
    model: "user"
  },
  "users.addEmail": {
    type: "create",
    model: "userEmail"
  },
  "users.deleteEmail": {
    type: "delete",
    model: "userEmail"
  },
  "users.setPrimaryEmail": {
    type: "update",
    model: "userEmail",
    implicit: function implicit(_ref25) {
      (0, _objectDestructuringEmpty3.default)(_ref25);
      return [{ desc: { type: "update", model: "userEmail" }, data: { isPrimary: undefined } }];
    }
  },
  "users.resendVerificationEmail": {
    type: "update",
    model: "userEmail"
  },
  "users.verifyEmail": {
    type: "update",
    model: "userEmail",
    implicit: function implicit(_ref26, _ref27) {
      var id = _ref27.id;
      (0, _objectDestructuringEmpty3.default)(_ref26);
      return [{ desc: { type: "update", model: "userEmail" }, data: { isVerified: undefined, id: id } }, { desc: { type: "create", model: "accountRole" }, data: {} }, { desc: { type: "delete", model: "userInvitation" }, data: {} }];
    }
  },
  "users.changePassword": {
    type: "update",
    model: "user"
  },
  "users.login": {
    type: "create",
    model: "user",
    onRoot: true
  },
  "users.updateRole": {
    type: "update",
    model: "accountRole",
    implicit: function implicit(_ref28) {
      var userId = _ref28.userId;
      return [{ desc: { type: "delete", model: "project", via: "project_users" }, data: { userId: userId } }, { desc: { type: "update", model: "subscription" }, data: { quantity: undefined } }];
    }
  },
  "users.disable": {
    type: "update",
    model: "accountRole",
    implicit: function implicit(_ref29) {
      var userId = _ref29.userId;
      return [{ desc: { type: "delete", model: "project", via: "project_users" }, data: { userId: userId } }, { desc: { type: "update", model: "subscription" }, data: { quantity: undefined } }, { desc: { type: "update", model: "accountRole" }, data: { userId: userId, role: undefined } }];
    }
  },
  "users.reEnable": {
    type: "update",
    model: "accountRole",
    implicit: function implicit(_ref30) {
      var userId = _ref30.userId;
      return [{ desc: { type: "delete", model: "project", via: "project_users" }, data: { userId: userId } }, { desc: { type: "update", model: "subscription" }, data: { quantity: undefined } }, { desc: { type: "update", model: "accountRole" }, data: { userId: userId, role: undefined } }];
    }
  },
  "users.delete": {
    type: "delete",
    model: "accountRole",
    implicit: function implicit(_ref31) {
      var userId = _ref31.userId;
      return [{ desc: { type: "delete", model: "project", via: "project_users" }, data: { userId: userId } }, { desc: { type: "update", model: "subscription" }, data: { quantity: undefined } }, { desc: { type: "update", model: "accountRole" }, data: { userId: userId, role: undefined } }];
    }
  },
  // no via, since to the receiving user it's like actually adding/removing a project
  "users.addProjectAccess": {
    type: "create",
    model: "project",
    implicit: function implicit(_ref32) {
      var userId = _ref32.userId,
          projectId = _ref32.projectId;
      return [{ desc: { type: "create", model: "user", via: "vw_all_project_users" }, data: { userId: userId, projectId: projectId } }];
    }
  },
  "users.setAsBillingContact": {
    type: "update",
    model: "accountRole",
    implicit: function implicit(_ref33) {
      var userId = _ref33.userId;
      return [{ desc: { type: "update", model: "accountRole" }, data: { userId: userId, isBillingContact: undefined } }];
    }
  },
  "users.revokeProjectAccess": {
    type: "delete",
    model: "project",
    implicit: function implicit(_ref34) {
      var userId = _ref34.userId,
          projectId = _ref34.projectId;
      return [{ desc: { type: "delete", model: "user", via: "vw_all_project_users" }, data: { userId: userId, projectId: projectId } }];
    }
  },
  "users.sendInvitation": {
    type: "create",
    model: "userInvitation",
    implicit: function implicit(_ref35, _ref36) {
      var type = _ref36.type;
      (0, _objectDestructuringEmpty3.default)(_ref35);
      return type === "user" ? [{ desc: { type: "create", model: "accountRole" }, data: {} }, { desc: { type: "update", model: "subscription" }, data: { quantity: undefined } }] : [];
    }
  },
  "users.revokeInvitation": {
    type: "delete",
    model: "userInvitation"
  },
  "users.resendInvitation": {
    type: "update",
    model: "userInvitation",
    implicit: function implicit(_ref37, _ref38) {
      (0, _objectDestructuringEmpty3.default)(_ref38);
      (0, _objectDestructuringEmpty3.default)(_ref37);
      return [{ desc: { type: "update", model: "userInvitation" }, data: { createdAt: undefined } }];
    }
  },
  "users.joinAccount": {
    type: "create",
    model: "user",
    implicit: function implicit(_ref39, _ref40) {
      var accountId = _ref39.accountId;
      (0, _objectDestructuringEmpty3.default)(_ref40);
      return [{ desc: { type: "create", model: "accountRole" }, data: { accountId: accountId } }, { desc: { type: "update", model: "subscription" }, data: { accountId: accountId, quantity: undefined } }, { desc: { type: "delete", model: "userInvitation" }, data: { accountId: accountId } }];
    }
  },
  "users.requestPasswordReset": {
    type: "update",
    model: "user"
  },
  "users.resetPassword": {
    type: "update",
    model: "user"
  },
  "users.updateUserSetting": {
    type: "update",
    model: "userSetting"
  },

  "hand.addCard": {
    type: "create",
    model: "handCard",
    implicit: function implicit(_ref41) {
      var id = _ref41.id,
          userId = _ref41.userId;
      return [{ desc: { type: "create", model: "card", via: "hand_cards" }, data: { cardId: id, userId: userId } }];
    }
  },
  "hand.addCards": {
    type: "create",
    model: "handCard",
    implicit: function implicit(_ref42) {
      var userId = _ref42.userId,
          ids = _ref42.ids;
      return ids.map(function (cardId) {
        return { desc: { type: "create", model: "card", via: "hand_cards" }, data: { cardId: cardId, userId: userId } };
      });
    }
  },
  "hand.addDeck": {
    type: "create",
    model: "handCard",
    implicit: function implicit(_ref43) {
      var userId = _ref43.userId;
      return [{ desc: { type: "create", model: "card", via: "hand_cards" }, data: { userId: userId } }];
    }
  },
  "hand.removeCard": {
    type: "delete",
    model: "handCard",
    implicit: function implicit(_ref44) {
      var id = _ref44.id,
          userId = _ref44.userId;
      return [{ desc: { type: "delete", model: "card", via: "hand_cards" }, data: { cardId: id, userId: userId } }];
    }
  },
  "hand.removeCards": {
    type: "delete",
    model: "handCard",
    implicit: function implicit(_ref45) {
      var userId = _ref45.userId,
          ids = _ref45.ids;
      return ids.map(function (cardId) {
        return { desc: { type: "delete", model: "card", via: "hand_cards" }, data: { cardId: cardId, userId: userId } };
      });
    }
  },

  "notifications.dismiss": {
    type: "delete",
    model: "unseenCardDiff",
    implicit: function implicit(_ref46) {
      var userId = _ref46.userId,
          cardId = _ref46.cardId,
          versions = _ref46.versions;
      return versions.map(function (version) {
        return { desc: { type: "delete", model: "unseenCardDiff" }, data: { userId: userId, cardId: cardId, version: version } };
      });
    }
  },
  "notifications.dismissMany": {
    type: "delete",
    model: "unseenCardDiff",
    implicit: function implicit(_ref47, _ref48) {
      var userId = _ref47.userId,
          ids = _ref47.ids;
      var _isOptimistic = _ref48._isOptimistic;
      return _isOptimistic ? [{
        desc: {
          type: "custom", fn: function fn(cache, optModelCache, data, mutationId, addOptimisticAction) {
            // we need to dismiss all cardDiffs of all versions for the cards, because that's what's happening on server
            var idsSet = new _set2.default(ids);
            (0, _keys2.default)(cache.unseenCardDiff).forEach(function (id) {
              var cardDiff = cache.unseenCardDiff[id].value;
              if (idsSet.has(cardDiff.cardId)) {
                addOptimisticAction(mutationId, {
                  type: "delete",
                  model: "unseenCardDiff"
                }, cardDiff, { _isOptimistic: true }, true);
              }
            });
          }
        }
      }] : [];
    }
  },
  "notifications.dismissResolvable": {
    type: "delete",
    model: "undismissedResolvable"
  },
  "notifications.seenResolvableEntries": {
    type: "delete",
    model: "resolvableEntry",
    via: "vw_unseen_resolvable_entries"
  },
  "notifications.dismissNonParticipatingResolvable": {
    type: "delete",
    model: "undismissedNonParticipatingResolvable"
  },

  "deckSubscriptions.addDeck": {
    type: "create",
    model: "deck",
    via: "deck_subscriptions"
  },
  "deckSubscriptions.removeDeck": {
    type: "delete",
    model: "deck",
    via: "deck_subscriptions"
  },

  "search.save": {
    type: "create",
    model: "savedSearch"
  },
  "search.delete": {
    type: "delete",
    model: "savedSearch"
  },

  "attachments.create": {
    type: "create",
    model: "attachment"
  },

  "files.update": { // action is only called by worker, no client directly
    type: "update",
    model: "file"
  },

  "releases.create": { // action is only called by release process, no client directly
    type: "create",
    model: "release",
    onRoot: true
  },

  "milestones.create": {
    type: "create",
    model: "milestone",
    convertDataForOptimistic: function convertDataForOptimistic(data) {
      return (// convert "2016-11-23" to {year: 2016, month: 11, day: 23}
        (0, _extends3.default)({}, data, {
          date: data.date.split("-").map(function (p, i) {
            return [parseInt(p, 10), ["year", "month", "day"][i]];
          }).reduce(function (m, _ref49) {
            var _ref50 = (0, _slicedToArray3.default)(_ref49, 2),
                v = _ref50[0],
                k = _ref50[1];

            m[k] = v;
            return m;
          }, {})
        })
      );
    }
  },
  "milestones.update": {
    type: "update",
    model: "milestone",
    convertDataForOptimistic: function convertDataForOptimistic(data) {
      return (// convert "2016-11-23" to {year: 2016, month: 11, day: 23}
        (0, _extends3.default)({}, data, {
          date: data.date.split("-").map(function (p, i) {
            return [parseInt(p, 10), ["year", "month", "day"][i]];
          }).reduce(function (m, _ref51) {
            var _ref52 = (0, _slicedToArray3.default)(_ref51, 2),
                v = _ref52[0],
                k = _ref52[1];

            m[k] = v;
            return m;
          }, {})
        })
      );
    }
  },
  "milestones.delete": {
    type: "delete",
    model: "milestone",
    implicit: function implicit() {
      return [{ desc: { type: "create", model: "cardHistory" }, data: {} }, { desc: { type: "create", model: "unseenCardDiff" }, data: {} }];
    }
  },
  "milestones.pin": {
    type: "create",
    model: "milestone",
    via: "pinned_milestones",
    implicit: function implicit(data) {
      return data.milestoneId ? [] : [{ desc: { type: "delete", model: "milestone", via: "pinned_milestones" }, data: data }];
    }
  },

  "subscriptions.addCreditCard": {
    type: "create",
    model: "creditCard",
    implicit: function implicit(_ref53) {
      var accountId = _ref53.accountId;
      return [{ desc: { type: "update", model: "account" }, data: { id: accountId, vatCountryCode: undefined } }, {
        desc: { type: "update", model: "balance" },
        data: { accountId: accountId, gift: undefined, actual: undefined, bonus: undefined, includesTaxPercent: undefined }
      }];
    }
  },
  "subscriptions.topUp": {
    type: "update",
    model: "balance",
    implicit: function implicit(_ref54) {
      var accountId = _ref54.accountId;
      return [{
        desc: { type: "update", model: "balance" },
        data: { accountId: accountId, gift: undefined, actual: undefined, bonus: undefined }
      }, {
        desc: { type: "update", model: "subscription" },
        data: { accountId: accountId, status: undefined, currentPeriodStart: undefined, currentPeriodEnd: undefined }
      }];
    }
  },
  "subscriptions.createSubscription": {
    type: "update",
    model: "balance",
    implicit: function implicit(_ref55) {
      var accountId = _ref55.accountId;
      return [{
        desc: { type: "update", model: "balance" },
        data: { accountId: accountId, gift: undefined, actual: undefined, bonus: undefined }
      }, {
        desc: { type: "update", model: "subscription" },
        data: {
          accountId: accountId,
          status: undefined,
          currentPeriodStart: undefined,
          currentPeriodEnd: undefined,
          viaStripe: true
        }
      }];
    }
  },
  "subscriptions.cancelSubscription": {
    type: "update",
    model: "subscription",
    implicit: function implicit(_ref56) {
      var accountId = _ref56.accountId;
      return [{
        desc: { type: "update", model: "subscription" },
        data: { accountId: accountId, status: undefined, canceledAt: undefined, canceledBy: undefined }
      }];
    }
  },
  "subscriptions.uncancelSubscription": {
    type: "update",
    model: "subscription",
    implicit: function implicit(_ref57) {
      var accountId = _ref57.accountId;
      return [{
        desc: { type: "update", model: "subscription" },
        data: { accountId: accountId, status: undefined, canceledAt: undefined, canceledBy: undefined }
      }];
    }
  }
};

exports.default = mutations;
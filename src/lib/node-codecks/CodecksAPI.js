'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = require('babel-runtime/core-js/object/entries');

var _entries2 = _interopRequireDefault(_entries);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _index = require('./lib/node-mate/index');

var _index2 = _interopRequireDefault(_index);

var _Mutations = require('./Mutations');

var _Mutations2 = _interopRequireDefault(_Mutations);

var _requireDir = require('require-dir');

var _requireDir2 = _interopRequireDefault(_requireDir);

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var context = (0, _requireDir2.default)("./shared_models");

var CodecksAPI = function () {
  function CodecksAPI(subdomain, token) {
    (0, _classCallCheck3.default)(this, CodecksAPI);

    this.subdomain = subdomain;
    this.token = token;

    this.sessionId = 'node-codecks';
    this.sensitiveKeys = new _set2.default(["password", "oldPassword", "token"]);

    var temp = [];
    var i = 0;
    (0, _keys2.default)(context).forEach(function (key) {
      temp[i] = context[key];
      i++;
    });

    this.serverRequest = this.serverRequest.bind(this);
    this.fetch = this.fetch.bind(this);
    this.strippedData = this.strippedData.bind(this);
    this.dispatcher = this.dispatcher.bind(this);

    this.descriptionList = temp;
    this.API = (0, _index2.default)(this.descriptionList, this.fetch, _Mutations2.default, this.dispatcher);
  }

  (0, _createClass3.default)(CodecksAPI, [{
    key: 'serverRequest',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(method, path, data) {
        var options;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                options = {
                  method: method,
                  uri: 'https://api.codecks.io' + path,
                  body: data,
                  json: true,
                  headers: {
                    'X-Auth-Token': this.token,
                    'X-Account': this.subdomain,
                    'Accept': 'application/vnd.codecks-v2'
                  }
                };
                return _context.abrupt('return', (0, _requestPromise2.default)(options));

              case 2:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function serverRequest(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      }

      return serverRequest;
    }()
  }, {
    key: 'fetch',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(query) {
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if ((0, _stringify2.default)(encodeURIComponent(query)).length > 2000) {
                  console.warn("query longer than 2000 bytes!", query);
                }
                this.serverRequest("post", "", { query: query }).then(function (res) {
                  return res;
                }).catch(function (error) {
                  throw error;
                });

              case 2:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function fetch(_x4) {
        return _ref2.apply(this, arguments);
      }

      return fetch;
    }()
  }, {
    key: 'strippedData',
    value: function strippedData(data) {
      var _this = this;

      return (0, _entries2.default)(data).reduce(function (m, _ref3) {
        var _ref4 = (0, _slicedToArray3.default)(_ref3, 2),
            key = _ref4[0],
            val = _ref4[1];

        m[key] = _this.sensitiveKeys.has(key) ? "*****" : val;
        return m;
      }, {});
    }
  }, {
    key: 'dispatcher',
    value: function () {
      var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(actionName, data) {
        var cleanData;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                cleanData = (0, _entries2.default)(data).reduce(function (m, _ref6) {
                  var _ref7 = (0, _slicedToArray3.default)(_ref6, 2),
                      key = _ref7[0],
                      val = _ref7[1];

                  m[key] = typeof val === "string" ? val.trim() : val;
                  return m;
                }, { sessionId: this.sessionId });
                return _context3.abrupt('return', this.serverRequest("post", '/dispatch/' + actionName, cleanData).then(function (res) {
                  console.log(res);
                  return res.payload;
                }).catch(function (error) {
                  throw error;
                }));

              case 2:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function dispatcher(_x5, _x6) {
        return _ref5.apply(this, arguments);
      }

      return dispatcher;
    }()
  }]);
  return CodecksAPI;
}();

exports.default = CodecksAPI;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var sortedStatusVals = exports.sortedStatusVals = ["unassigned", "assigned", "started", "snoozing", "blocked", "review", "done", "archived", "deleted"];

var statusToSortVal = sortedStatusVals.reduce(function (memo, val, i) {
  memo[val] = i;
  return memo;
}, {});

var statusColors = exports.statusColors = {
  snoozing: "#7d1274",
  blocked: "#c01b1b",
  assigned: "#a8925d",
  unassigned: "#828282",
  review: "#159590",
  done: "#31d367",
  started: "#2569ad",
  archived: "#291600",
  deleted: "#43282a",
  not_started: "#828282"
};

var statusForCard = exports.statusForCard = function statusForCard(card) {
  if (card.visibility === "archived") return "archived";
  if (card.visibility === "deleted") return "deleted";
  if (card.$meta.exists("openBlockResolvables")) return "blocked";
  if (card.$meta.exists("openReviewResolvables")) return "review";
  if (card.status === "not_started") return card.assignee ? "assigned" : "unassigned";
  return card.status;
};

var statusToLabel = exports.statusToLabel = {
  snoozing: "Snoozing",
  blocked: "Blocked",
  assigned: "Assigned",
  unassigned: "Unassigned",
  review: "Review",
  done: "Done",
  started: "Started",
  archived: "Archived",
  deleted: "Deleted",
  not_started: "Not started"
};

// x cards are [PHRASE]
var statusToPhrase = exports.statusToPhrase = {
  snoozing: "snoozing",
  blocked: "blocked",
  assigned: "assigned",
  unassigned: "unassigned",
  review: "in review",
  done: "done",
  started: "started",
  archived: "archived",
  deleted: "deleted",
  not_started: "not started"
};

var statusForCardLabel = exports.statusForCardLabel = function statusForCardLabel(card) {
  return statusToLabel[statusForCard(card)];
};
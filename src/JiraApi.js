const config = require('./config.json');
import rp from 'request-promise';

export default class JiraAPI {

  constructor (domain, token) {
    this.domain = domain;
    this.token = token;
  }

  /**
   *
   * @param method
   * @param path
   * @param data
   * @returns {Promise.<void>}
   */
  async serverRequest (method, path, data) {
    let options = {
      method: method,
      uri: 'https://' + this.domain + '.atlassian.net/rest/api/2/' + path,
      body: data,
      json: true,
      headers: {
        'Authorization': this.token
      }
    };

    return rp(options);
  }

  /**
   *
   * @param method
   * @param path
   * @param data
   * @returns {Promise.<void>}
   */
  async serverAgileRequest (method, path, data) {
    let options = {
      method: method,
      uri: 'https://' + this.domain + '.atlassian.net/rest/agile/1.0/' + path,
      body: data,
      json: true,
      headers: {
        'Authorization': this.token
      }
    };

    return rp(options);
  }

  /**
   *
   * @returns {Promise}
   */
  async getProjects () {
    return this.serverRequest('get', 'project', '');
  }

  /**
   *
   * @returns {Promise}
   */
  async getStatuses () {
    return this.serverRequest('get', 'statuscategory', '');
  }

  /**
   *
   * @param projectKey
   * @returns {Promise.<Array>}
   */
  async getEpics (projectKey) {
    let epics = [];

    // customfield_10006 is the short name of the epic
    let data = {
      jql: 'project = ' + projectKey + ' AND type = Epic',
      fields: [ 'summary', 'customfield_10006' ]
    };
    await this.serverRequest('post', 'search/', data)
      .then(res => {
        epics = res.issues.length === 0 ? [] : epics.concat(res.issues);
      })
      .catch(err => {throw err});
    return epics;
  }

  /**
   *
   * @param projectKey
   * @returns {Promise}
   */
  async getProjectVersions (projectKey) {
    return this.serverRequest('get', 'project/' + projectKey + '/versions', '');
  }

  /**
   *
   * @param projectKey
   * @returns {Promise.<Array>}
   */
  async getSprints (projectKey) {
    let sprints = [];
    let promises = [];

    this.serverAgileRequest('get', 'board?projectKeyOrId=' + projectKey, '')
      .then(res => {
        for (let board of res.values) {
          promises.push(this.serverAgileRequest('get', 'board/' + board.id + '/sprint')
            .then(res => {sprints = sprints.concat(res.values)}));
        }
      });
    await Promise.all(promises);
    return sprints;
  }

  /**
   *
   * @param projectKey
   * @returns {Promise.<Array>}
   */
  async getTasks (projectKey) {
    let tasks = [];
    let tasksNb = 0;

    // We first get the number of issues in the project
    let data = {
      jql: 'project = ' + projectKey + ' AND (type = Task OR type = Sub-Task)',
      maxResults: 1,
      fields: [ 'summary' ]
    };
    await this.serverRequest('post', 'search/', data).then(res => tasksNb = res.total);

    // We then divide this by 1000 to get the number of queries we need to do. 1000 being the max nb we can get per query
    let queriesNb = tasksNb / 1000;

    // We do our X queries and add the results to tasks
    for (let i = 0; i < queriesNb; i++) {
      data = {
        jql: 'project = ' + projectKey + ' AND (type = Task OR type = Sub-Task)',
        maxResults: 1000,
        startAt: i * 1000,
        fields: [ 'summary', 'description', 'status', 'assignee', 'parent', 'labels', 'fixVersions', 'customfield_10004', 'resolutiondate' ]
      };
      await this.serverRequest('post', 'search/', data).then(res => tasks = tasks.concat(res.issues));
    }

    // We finally return the tasks
    return tasks;
  }
}
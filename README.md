# Jira Exporter to Codecks.io

This is a work in progress tool, allowing you to export your Jira projects to Codecks.io.

## How it works

This is fairly simple. 

1. We first GET all the Jira projects
2. Foreach of them, we create a Codecks project, then we GET theirs Epics, Versions and Tasks
3. We convert the Epics to Codecks decks and create the decks
4. We convert the Versions to Codecks milestones, and create the milestones
5. We convert the tasks and sub-tasks to Codecks cards, and create the cards

The card creation is a bit tricky. A task (Jira) and a card (Codecks) contain a huge amount of information.

* Sub-tasks are converted to cards (there is no sub-card system in Codecks), and get the same milestone and deck as their parent. Their title is changed, we add parent.summary before their normal title.
* The task workflow status is converted to its Codecks counterpart thanks to the provided dictionary
* The new card is archived if the task was 'done' more than 48 hours ago
* The card content is parsed to replace Jira users' references to their Codecks counterparts

## Config

The config.json file contains:
* Jira and Codecks security configuration (tokens)
* A list of users with their JiraKey and CodecksId 
* A dictionary liking Jira workflow statuses to Codecks workflow

You will need to complete this configuration manually before launching the tool. In the future the configuration may be done with a dedicated UI.

## Limitations

* This project has not (yet) been tested on a lot of projects. Only basic Kanban and Software Jira projects have been tested. This may not work with bug tracking projects for example.
However, Codecks is not really suited for such tasks, thus I don't see this as a big problem
* Sprints are not considered in the export, for a simple reason : cards in Codecks can only have 1 Milestone
* Attachments are not exported


const gulp = require('gulp');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('default', () =>
  gulp.src('src/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: [ 'es2015' ],
      plugins: [ 'transform-runtime', 'transform-async-to-generator', 'transform-object-rest-spread' ]
    }))
    .on('error', onError)
    .pipe(sourcemaps.write('.', {
      includeContent: false,
      sourceRoot: '../src'
    }))
    .pipe(gulp.dest('build'))
);

gulp.task('copy', () =>
  gulp.src('src/**/*.json')
    .pipe(gulp.dest('build'))
);

gulp.task('watch', function () {
  gulp.watch('src/**/*.js', [ 'default', 'copy' ]);
});

function onError (err) {
  console.log(err);
  this.emit('end');
}

